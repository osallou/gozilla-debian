FROM golang:latest

ADD ./* /root/
WORKDIR /root/
RUN go build

FROM debian:latest
WORKDIR /root/
RUN apt-get update && apt-get install -y wget gnupg
RUN wget -qO - https://www.aptly.info/pubkey.txt | apt-key add -
RUN echo "deb http://repo.aptly.info/ squeeze main" > /etc/apt/sources.list.d/aptly.list
RUN apt-get update && apt-get install -y aptly
COPY --from=0 /root/gozilla-debian /usr/bin/
COPY --from=0 /root/buildrepo.sh /usr/bin/
COPY --from=0 /root/gozilla.yml /root/
