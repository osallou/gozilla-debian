package main

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/streadway/amqp"

	mongo "go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	gozilla "gitlab.inria.fr/osallou/gozilla-lib"

	elasticsearch "github.com/elastic/go-elasticsearch"
)

// Version of server
var Version string

type favContextKey string

// UploadFile uploads a file
func UploadFile(uri string, path string) (*http.Request, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	fi, err := file.Stat()
	if err != nil {
		return nil, err
	}
	file.Close()

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", fi.Name())
	if err != nil {
		return nil, err
	}
	part.Write(fileContents)

	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, reqErr := http.NewRequest("PUT", uri, body)
	if reqErr == nil {
		req.Header.Set("Content-Type", writer.FormDataContentType())
	}

	return req, reqErr
}

// DownloadFile downloads a file
func DownloadFile(filepath string, url string) error {
	log.Debug().Str("url", url).Str("path", filepath).Msg("download")
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	return err
}

// PublishHandler manage publish requests
type PublishHandler struct {
	Goz gozilla.GozContext
}

func (p PublishHandler) buildRepo(repo string, dir string, distrib string, component string, archs string) []string {
	// execute buildrepo.sh
	cmd := exec.Command(p.Goz.Config.Debian.Script, repo, distrib, component, archs, dir, p.Goz.Config.Gpg.Key, p.Goz.Config.Gpg.Passphrase)
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		log.Error().Str("repo", repo).Err(err).Msg("buildrepo error")
	}
	log.Debug().Msgf("Logs: %s", stdoutStderr)

	files := make([]string, 0)
	// parse /root/.aptly/public
	aptly := p.Goz.Config.Debian.Aptly
	filepath.Walk(aptly,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if strings.HasPrefix(path, aptly) {
				path = strings.Replace(path, aptly, "", 1)
			}
			files = append(files, path)
			return nil
		})

	// return list of files
	return files
}

func (p PublishHandler) publish(curpack gozilla.PackageVersion) {
	log.Debug().Str("package", curpack.Package).Msg("create debian repo")

	packrepo := gozilla.Repo{
		Subject: curpack.Subject,
		ID:      curpack.Repo,
	}
	vpacks := make([]gozilla.PackageVersion, 0)
	repo, _ := packrepo.Get(p.Goz)
	repoPacks, _ := repo.ListPackages(p.Goz)
	for _, repoPack := range repoPacks {
		repoPackVersions, _ := repoPack.GetVersions(p.Goz)
		for _, repoPackVersion := range repoPackVersions {
			vpacks = append(vpacks, repoPackVersion)
		}
	}
	if len(vpacks) == 0 {
		log.Error().Msg("no version found")
		return
	}

	/*
		pack := gozilla.Package{
			ID:      curpack.Package,
			Subject: curpack.Subject,
			Repo:    curpack.Repo,
		}
		vpacks, vpacksErr := pack.GetVersions(p.Goz)
		if vpacksErr != nil {
			log.Error().Err(vpacksErr).Msg("no version found")
			return
		}
	*/
	log.Debug().Msgf("Repo versions: %+v", vpacks)

	repoFiles := make([]gozilla.FileObject, 0)
	for _, vpack := range vpacks {
		if !vpack.Published && vpack.Version != curpack.Version {
			continue
		}
		vfiles, vErr := vpack.Files(p.Goz)
		if vErr != nil {
			log.Error().Err(vErr).Msg("file list error")
			continue
		}
		repoFiles = append(repoFiles, vfiles...)
	}
	log.Debug().Msgf("Repo files: %+v", repoFiles)

	type Repo struct {
		Component string
		Archs     string
		Files     []gozilla.FileObject
	}

	// Create 1 repo per distribution
	repos := make(map[string]Repo)
	for _, repoFile := range repoFiles {
		if repoFile.GetType() != gozilla.DebianPackage {
			continue
		}
		extras := repoFile.GetExtras()
		log.Debug().Msgf("extras %+v", extras)
		if _, ok := extras["distribution"]; ok {
			d := extras["distribution"].(string)
			if _, ok := repos[d]; !ok {
				repos[d] = Repo{
					Component: extras["component"].(string),
					Archs:     extras["architectures"].(string),
					Files:     make([]gozilla.FileObject, 0),
				}
			}
			repo := repos[d]
			repo.Files = append(repo.Files, repoFile)
			repos[d] = repo
		}
	}

	log.Debug().Msgf("Repos: %+v", repos)

	// TODO create repo per distrib/component matrix
	// Here, will work only if same component for all packages
	for distrib, repo := range repos {
		hasFiles := false
		dir, err := ioutil.TempDir("", distrib)
		if err != nil {
			log.Error().Msg("could not create temp dir")
			return
		}
		for _, repoFile := range repo.Files {
			fh, fErr := p.Goz.Storage.GetObjectURL(p.Goz, repoFile, 60)
			if fErr != nil {
				log.Error().Str("file", repoFile.GetPath()).Msg("could not get a temp url")
				continue
			}
			downErr := DownloadFile(filepath.Join(dir, repoFile.GetName()), fh)
			if downErr != nil {
				log.Error().Err(downErr).Str("url", fh).Msg("Failed to download file")
				continue
			}
			hasFiles = true
		}
		if !hasFiles {
			log.Error().Str("distrib", distrib).Msg("no file to manage, skipping repo")
			continue
		}
		// Now should create/build repo
		filesToUpload := p.buildRepo(distrib, dir, distrib, repo.Component, repo.Archs)
		// Now should send back to storage filesToUpload
		for _, fileToUpload := range filesToUpload {
			filePath := filepath.Join(p.Goz.Config.Debian.Aptly, fileToUpload)
			fo := gozilla.NewDebianRepoFileObject(curpack.Subject, curpack.Repo, distrib, fileToUpload)
			stat, statErr := os.Stat(filePath)
			if fileToUpload == "" || statErr != nil || stat.IsDir() {
				log.Error().Msgf("file %s", fileToUpload)
				continue
			}
			fh, fhErr := p.Goz.Storage.SaveObject(p.Goz, fo, 60)
			if fhErr != nil {
				log.Error().Msgf("Failed to get an url for upload: %+v", fh)
				continue
			}
			log.Debug().Str("url", fh).Str("path", filePath).Msg("upload")
			req, err := UploadFile(fh, filePath)
			if err != nil {
				log.Error().Str("url", fh).Str("path", filePath).Msg("failed to upload")
				continue
			}
			client := &http.Client{}
			resp, err := client.Do(req)
			if err != nil {
				log.Error().Str("url", fh).Str("path", filePath).Msg("failed to upload")
			}
			resp.Body.Close()

		}

		os.RemoveAll(dir)
	}

}

func main() {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("GOZ_DEBUG") == "1" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	config := gozilla.LoadConfig()

	mongoClient, err := mongo.NewClient(mongoOptions.Client().ApplyURI(config.Mongo.URL))
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}
	ctx, cancelMongo := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelMongo()

	err = mongoClient.Connect(ctx)
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}

	storage, storageErr := gozilla.NewStorageHandler(config)
	if storageErr != nil {
		log.Error().Err(storageErr).Msg("storage error")
		os.Exit(1)
	}

	esCfg := elasticsearch.Config{
		Addresses: config.Elastic,
	}
	es, err := elasticsearch.NewClient(esCfg)
	if err != nil {
		log.Error().Err(err).Msgf("Error creating the client: %s", err)
		os.Exit(1)
	}

	amqpHandler, amqpErr := gozilla.NewAmqpHandler(config)
	if amqpErr != nil {
		log.Error().Err(amqpErr).Msg("rabbitmq error")
		os.Exit(1)
	}
	defer amqpHandler.Conn.Close()

	msgs, consumeErr := amqpHandler.Ch.Consume(
		amqpHandler.DebianQueue, // queue
		"",                      // consumer
		false,                   // auto-ack
		false,                   // exclusive
		false,                   // no-local
		false,                   // no-wait
		nil,                     // args
	)

	if consumeErr != nil {
		log.Error().Err(consumeErr).Msg("failed to get messages")
		panic("failed to get messages")
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGTERM)

	go func(connection *amqp.Connection, channel *amqp.Channel) {
		sig := <-sigs
		channel.Close()
		connection.Close()
		log.Warn().Msgf("Closing AMQP channel and connection after signal %s", sig.String())
		log.Warn().Msg("Ready for shutdown")
	}(amqpHandler.Conn, amqpHandler.Ch)

	gozCtx := gozilla.GozContext{
		Mongo:       mongoClient,
		Config:      config,
		Storage:     storage,
		AmqpHandler: amqpHandler,
		Elastic:     es,
	}

	pHandler := PublishHandler{
		Goz: gozCtx,
	}

	forever := make(chan bool)

	go func(p PublishHandler) {
		log.Debug().Msgf("listen for messages on %s", amqpHandler.DebianQueue)
		for d := range msgs {
			var pack gozilla.PackageVersion
			err := json.Unmarshal(d.Body, &pack)
			if err == nil {
				p.publish(pack)

			} else {
				log.Error().Str("body", string(d.Body)).Msg("failed to decode message")
			}
			d.Ack(true)
		}
	}(pHandler)

	<-forever

}

func test() {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("GOZ_DEBUG") == "1" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	config := gozilla.LoadConfig()

	mongoClient, err := mongo.NewClient(mongoOptions.Client().ApplyURI(config.Mongo.URL))
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}
	ctx, cancelMongo := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelMongo()

	err = mongoClient.Connect(ctx)
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}

	storage, storageErr := gozilla.NewStorageHandler(config)
	if storageErr != nil {
		panic(storageErr)
	}

	esCfg := elasticsearch.Config{
		Addresses: config.Elastic,
	}
	es, err := elasticsearch.NewClient(esCfg)
	if err != nil {
		log.Error().Msgf("Error creating the client: %s", err)
		panic(err)
	}

	gozCtx := gozilla.GozContext{
		Mongo:   mongoClient,
		Config:  config,
		Storage: storage,
		Elastic: es,
	}

	pHandler := PublishHandler{
		Goz: gozCtx,
	}

	pack := gozilla.PackageVersion{
		Subject: "test",
		Repo:    "testmvn",
		Package: "biomaj3",
		Version: "3",
	}
	pHandler.publish(pack)

}
