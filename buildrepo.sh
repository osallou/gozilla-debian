#!/bin/bash

set -e
rm -rf /root/.aptly/*

# $1 = package
# $2 = distrib
# $3 = component
# $4 = archs
# $5 = dir
# $6 = gpg key id
# $7 = gpg passphrase


aptly repo create $1

aptly repo add $1 $5

aptly publish -distribution=$2 -component=$3 -architectures=$4 -gpg-key=$6 -passphrase=$7 -batch repo $1 